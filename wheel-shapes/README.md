# README.md for laser cut wheel shapes

In this folder you'll find the files and instructions for lasercutting the wheel shapes that can be attached to the [Playing with the Sun motor](https://docs.playingwiththesun.org/Motor/). 

The wheel is a not-so-round plwwood (3mm) or acrylic shape that can be fit over the wheel hub. This is used for activities like drawing machines.

The wheel separator is a small round shape that fits over the wheel hubs. Primarily they are used for the cable crawler activity, wherein 2 of them are sandwiched between two larger wheel shapes so that they act like the center of a pulley that rides on the cable.

These instructions should supplement the [motor build instructions](https://docs.playingwiththesun.org/Motor/). 
