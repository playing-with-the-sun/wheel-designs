# Solar Panel Frame 3.0

Solar panel mounts on top, frame provides mount points for strap.
Use 3mm wood or acrylic (4mm is too thick for push rivets).

1 each XT30PW-M XT30PW-F
Use VHB tape to mount panel onto frame.
.5 watt SEEED Studios Solar Panel
https://www.mouser.dk/ProductDetail/Seeed-Studio/313070004?qs=SElPoaY2y5IR7rJqVJryrQ%3D%3D

## Lasercutter settings (90 watt laser w 3 mm birch ply)
Cut line 50mm @ 70% 2 pass
Frame line is 350mm @ 20
Text is 350mm @ 20 Fill

See following for instructions:
https://resources.playingwiththesun.org/Solar-Panel/
