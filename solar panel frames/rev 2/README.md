# Solar Panel Frame 2.0

Solar panel mounts on top, frame provides mount points for strap.
Use 3mm ply or acrylic (4mm is too thick for push rivets).

## Lasercutter settings (90 watt laser w 3 mm birch ply)
Black (cut) line 60 mm / sec  @ 50% 2 pass
Red (engrave) line 200 mm / sec @ 10 % 1 pass

## Revisions

- 0.7  Slots for zip tie mounting of XT30 connector (And remove old frame.)
- 0.6  Solid plate for mounting panel with VHB. Increase hole offset for clearance from panel..
- 0.5  Switch to 4 corner mount points.
- 0.4  Add logo, cut corners of XT30 mount, fine tune holes.
- 0.3  Add XT30 footprint, cut out for glueing / mounting directly over 
solder pad.
- 0.2  Aligned holes for strap.
- 0.1  Tested with 4mm ply, too thick.  Holes are misaligned / too far 
apart, but right diameter (4mm).
