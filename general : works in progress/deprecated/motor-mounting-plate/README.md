# README.md for laser-cut motor cover

This is a stub for the readme that explains how to cut the motor covers, the files for which should be in this directory. 

These instructions should supplement the [motor build instructions](https://docs.playingwiththesun.org/Motor/). 