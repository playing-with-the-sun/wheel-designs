# Playing with the Sun Construction Kit Digital Fabrication Files

This repository contains digital files necessary to fabricate 3D printed and lasercut elements of the [Playing with the Sun Construction Kit](https://docs.playingwiththesun.org/Const-Kit-Overview/). 

## Crank Slider Wheel Caps
These can be mounted on top of the wheel hubs with a piece of plumber's strap inserted, so as to make the wheel hubs turn another piece of strap like a crank slider.

## Dummy Flash Light Battery
3D Printed battery like cylinder that can take an XT30 connector and feed it's power in to a voltage regulator, the output of which can be used to drive a flashlight. This allows you to use the flashlight with any of the power sources availabling in the playing with the sun system.

## solar panel frames
Laser cut backing frames for solar panels.

## wheel hubs
Mounts permanently onto the motor, and can be used to drive different wheel shapes and the crank slider wheel cap. We find that printing them solid / 100% infill makes them tougher, as they get a fair amount of abuse in use.

## wheel-shapes 
Colored acrylic makes for nice shapes that generate shadows in the sun. Nice for doing rotating sculptures.

## general / works in progress

### Hole-grid-base.svg
This is a way to cut a grid of holes that match the plumber strap on a full size, 
30x50 3mm sheet of lasercutting plywood. Holes are 4.2mm and offset in inkscape using create tiled clones with a horizontal / vert  offset of 350.4761%.

### Deprecated (No longer used in the current revision)

#### Motor Mounting Plate

This is essentially a spacer that allows us to use the screws that work with the motor housing. Without it, the long screws can go too deep into the front face of the motor housing. Cutting this spacer gives us the 2-4 mm offset needed, and is easier than finding and sourcing shorter screws!
Note: Now we just cut 2-4 mm off of ends off the screws that come in the motor housing, and it seems to work just fine.
