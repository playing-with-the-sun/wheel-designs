# README.md for 3D printed wheel hubs

This is a stub for the readme that explains how to print the wheel hubs, the files for which should be in this directory. 

These instructions should supplement the [motor build instructions](https://docs.playingwiththesun.org/Motor/). 